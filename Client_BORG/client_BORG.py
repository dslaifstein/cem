
import asyncio
import websockets
import os
import aiofiles
import json


async def read_json_file(file_path):
    try:
        async with aiofiles.open(file_path, "r") as file:
            data = await file.read()
            return data
    except FileNotFoundError:
        return None

async def send_measured_json(file_path, websocket):
    try:
        current_time = os.path.getmtime(file_path)

        while True:
            await asyncio.sleep(1)

            modified_time = os.path.getmtime(file_path)

            if current_time != modified_time:
                current_time = modified_time
                measured_json_data = await read_json_file(file_path)
                if measured_json_data:
                    await websocket.send(measured_json_data)
                    print(f"Sent 'measured_BORG.json' data to the server.")

    except websockets.ConnectionClosed:
        print("Connection to the server closed.")

async def receive_setpoint_json(file_path, websocket):
    try:
        while True:
            setpoint_json_data = await websocket.recv()
            if setpoint_json_data:
                with open(file_path, "w") as file:
                    file.write(setpoint_json_data)
				# Extract the file name from the path
                file_name = os.path.basename(file_path)
                print(f"Received '{file_name}' from the server and saved it.")	

    except websockets.ConnectionClosed:
        print("Connection to the server closed.")


async def main():
    measured_file_path = os.path.expanduser("/home/sdrohan19/BORG/measured_BORG.json")  # Adjust the file path accordingly
    received_file_path = os.path.expanduser("/home/sdrohan19/BORG/sp_BORG.json")  # Adjust the file path accordingly
	# Create the folder if it doesn't exist
    #if not os.path.exists(received_file_path):
    #    os.makedirs(received_file_path)
	

    async with websockets.connect("ws://131.180.178.183:8080") as websocket:
        print("Connected to WebSocket server.")

        await websocket.send("BORG_456")  # Change the password to match the server's password for client_B

        # Send the initial "measured.json" data to the server
        measured_json_data = await read_json_file(measured_file_path)
        if measured_json_data:
            await websocket.send(measured_json_data)
            print("Sent initial 'meaured_BORG.json' file to the server.")


        # Create tasks for sending "meaured_BORG.json" and receiving "sp_BORG.json" concurrently
        tasks = [
            asyncio.create_task(send_measured_json(measured_file_path, websocket)),
            asyncio.create_task(receive_setpoint_json(received_file_path, websocket))
        ]

        # Wait for all tasks to complete
        await asyncio.gather(*tasks)

if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())

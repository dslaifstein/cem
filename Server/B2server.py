import asyncio
import websockets
import os
import aiofiles
import json

# Dictionary to store connected clients
clients = {}

async def read_json_file(file_path):
    try:
        async with aiofiles.open(file_path, "r") as file:
            data = await file.read()
            return data
    except FileNotFoundError:
        return None

async def handle_client(websocket, path):
    try:
        # Send the type request to the client
        await websocket.send(":")   #("Please enter the password:")
        password = await websocket.recv()
        
        # Check password for client_PRE
        if password == "PRE_123":
            clients[websocket] = "PRE"
            print(f"Client {id(websocket)} (client_PRE) connected.")
            folder_path = os.path.expanduser(f"/home/grituraj/CEM/PRE_sp")
        # Check password for client_BORG
        elif password == "BORG_456":
            clients[websocket] = "BORG"
            print(f"Client {id(websocket)} (client_BORG) connected.")
            folder_path = os.path.expanduser(f"/home/grituraj/CEM/BORG_sp")
        else:
            print(f"Client {id(websocket)} attempted to connect with an incorrect password. Closing connection.")
            await websocket.close()
            return


        #Send the appropriate JSON file to the client
        sp_file_path = os.path.join(folder_path, f"sp_{clients[websocket]}.json")  #sp means set point...one the name of setpoints files
        sp_data = await read_json_file(sp_file_path)
        if sp_data:
            await websocket.send(sp_data)
            print(f"Sent '{sp_file_path}' to client {id(websocket)}.")

        while True:
            # Receive and save the JSON data from the client
            received_data = await websocket.recv()
            try:
                received_json = json.loads(received_data)
                mea_json_file_path = os.path.join(folder_path, f"measured_{clients[websocket]}.json") #mea.... for measured json files
                with open(mea_json_file_path, "w") as file:
                    json.dump(received_json, file)
                print(f"Received and saved '{mea_json_file_path}' from client {id(websocket)}")
            except json.JSONDecodeError:
                print(f"Invalid JSON data received from client {id(websocket)}.")

    except websockets.ConnectionClosed:
        # Remove the client from the dictionary when the connection is closed
        if websocket in clients:
            del clients[websocket]
        print(f"Client {id(websocket)} disconnected.")


async def watch_file_changes():
    sp_p_json_path = os.path.expanduser("/home/grituraj/CEM/PRE_sp/sp_PRE.json")
    sp_b_json_path = os.path.expanduser("/home/grituraj/CEM/BORG_sp/sp_BORG.json")
    P_current_time = os.path.getmtime(sp_p_json_path)
    B_current_time = os.path.getmtime(sp_b_json_path)

    while True:
        await asyncio.sleep(1)  # Check for changes every 1 second

        # Check if "sp_PRE.json" has been modified
        P_modified_time = os.path.getmtime(sp_p_json_path)
		
        if P_current_time != P_modified_time:
            P_current_time = P_modified_time
            sp_p_json_data = await read_json_file(sp_p_json_path)
            if sp_p_json_data:
                #Send the updated "sp_p.json" file to P client
                for client, client_type in clients.items():
                    if client_type == "PRE":
                       await client.send(sp_p_json_data)
                       print(f"Sent updated '{sp_p_json_path}' to client {id(client)} (client_PRE).")
			 		
		# Check if "sp_b.json" has been modified
        B_modified_time = os.path.getmtime(sp_b_json_path)
		
        if B_current_time != B_modified_time:
            B_current_time = B_modified_time
            sp_b_json_data = await read_json_file(sp_b_json_path)
            if sp_b_json_data:
                #Send the updated "sp_BORG.json" file to P client
                for client, client_type in clients.items():
                    if client_type == "BORG":
                        await client.send(sp_b_json_data)
                        print(f"Sent updated '{sp_p_json_path}' to client {id(client)} (client_BORG).")



async def main():
    server = await websockets.serve(handle_client, "131.180.178.183", 8080)

    print("WebSocket server started at ws://flexinet.ewi.tudelft.nl:8080")
    asyncio.create_task(watch_file_changes())  # Start watching for file changes
    await server.wait_closed()

if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())

